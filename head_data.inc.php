<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 20/03/2018
 * Time: 23:33
 */
?>

<script
    src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
<script src="/js/kangoroo.js"></script>

<link rel="stylesheet" href="css/style.css">
<link href="https://fonts.googleapis.com/css?family=Lobster|Montserrat|Roboto|Quicksand" rel="stylesheet">