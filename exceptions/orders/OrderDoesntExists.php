<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 31/03/2018
 * Time: 12:48
 */
require_once(__DIR__.'/OrderException.php');

class OrderDoesntExists extends OrderException {
}