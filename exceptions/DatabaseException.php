<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 20/03/2018
 * Time: 19:18
 */
require_once(__DIR__ . '/KangorooException.php');

abstract class DatabaseException extends KangorooException
{
    use ConfidentialException;
}