<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 20/03/2018
 * Time: 19:12
 */
require_once(__DIR__.'/ShowableException.php');
require_once(__DIR__.'/ConfidentialException.php');

class KangorooException extends Exception
{
    public function __construct($message, $code = 0)
    {
        parent::__construct($message, $code);
        //motherofhandlerexceptions
    }

    public function __toString()
    {
        return $this->message;
    }
}
