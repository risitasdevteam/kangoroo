<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/03/2018
 * Time: 23:02
 */
abstract class OrderFeature
{
    const one_full_page = 0;
    const a_half_page = 1;
    const a_quarter_page = 2;
}