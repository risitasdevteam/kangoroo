<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/03/2018
 * Time: 15:38
 */
abstract class OrderStatusEnum
{
    const waiting_for_provider = 0;
    const provider_submitted_data = 1;
    const admins_are_looking = 2;
    const admins_sent_back_data = 3;
    const provider_approved = 4; //si invalide alors retour 1
    const completed_waiting_for_next_year = 5;
}