<?php
require_once(__DIR__.'/OrderFeature.enum.php');
require_once(__DIR__.'/OrderStatus.class.php');
require_once(__DIR__.'/OrderStatusEnum.enum.php');
require_once(__DIR__.'/../exceptions/orders/OrderDoesntExists.php');

define("UPLOADS_ROOT_PATH", __DIR__.'/../../uploads/');
define("UPLOADS_PROVIDER_PATH", UPLOADS_ROOT_PATH.'%s/');
define("UPLOADS_PROVIDER_SPECIFIC_YEAR_PATH", UPLOADS_PROVIDER_PATH.'%u/');
define("UPLOADS_PROVIDER_SUBMITTED_OWN_FILES_PATH", UPLOADS_PROVIDER_SPECIFIC_YEAR_PATH.'provider/');
define("UPLOADS_PROVIDER_SUBMITTED_STAFF_FILES_PATH", UPLOADS_PROVIDER_SPECIFIC_YEAR_PATH.'staff/');

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 18/03/2018
 * Time: 21:47
 */
class Order
{
    public $unique_id; //l=10, 5 random hashed bytes
    public $provider_unique_id; //l=10, 5 random hashed bytes
    public $feature; //OrderFeature json array
    public $status_history; //OrderStatus.class array
    public $year; //Order submission year

    private $exists = false;

    private static $allowed_variables = array('unique_id', 'provider_unique_id', 'feature', 'status_history', 'year');
    private static $allowed_file_types = array('pdf', 'jpg', 'jpeg', 'png', 'tiff', 'xls', 'doc', 'ods');

    public function __construct($array){
        foreach ($array as $key=>$value){
            if (in_array($key, self::$allowed_variables)){
                switch($key){
                    case self::$allowed_variables[0]:
                        $this->unique_id = $value;
                        break;

                    case self::$allowed_variables[1]:
                        $this->provider_unique_id = $value;
                        break;

                    case self::$allowed_variables[2]:
                        $this->feature = $value;
                        break;

                    case self::$allowed_variables[3]:
                        $this->status_history = $value;
                        break;

                    case self::$allowed_variables[4]:
                        $this->year = $value;
                        break;
                }
            }else{
                throw new KangorooException('Argument "'.$key.'" isn\'t allowed.');
            }
        }
    }

    public static function create($order, $upload_files = null){
        if (!$order instanceof Order){
            throw new KangorooException('Parameter $order must be an instance of Order.class');
        }

        $order->provider_unique_id = (strlen($order->provider_unique_id) > 0 ? $order->provider_unique_id : User::get_uid_from_session());
        $order->year = (isset($order->year) ? $order->year : date('Y'));
        $exists = $order->exists(false);
        if ($exists){
            throw new KangorooException('This order seem to have been already registered, unique id is the key.');
        }

        if ($upload_files != null){
            $order->upload_provider_files($upload_files); //throw error if something bad occurs
        }

        $order->unique_id = bin2hex(openssl_random_pseudo_bytes(5));

        $status = new OrderStatus();
        $status->status = OrderStatusEnum::provider_submitted_data;
        $status->timestamp = time();

        $order->feature = (isset($order->feature) ? json_encode($order->feature) : "");

        $order->status_history = array($status);
        $order->status_history = (isset($order->status_history) ? json_encode($order->status_history) : ""); //empty array

        $data2insert = [
            self::$allowed_variables[0] => $order->unique_id,
            self::$allowed_variables[1] => $order->provider_unique_id,
            self::$allowed_variables[2] => $order->feature,
            self::$allowed_variables[3] => $order->status_history,
            self::$allowed_variables[4] => $order->year
        ];

        if (!isset($data2insert[self::$allowed_variables[1]])){
            throw new KangorooException('dd');
        }

        return SQLUtil::insert(ORDERS_TABLE, $data2insert);
    }

    public function exists($rebind = false){
        $where = array(); //we adjust research as much as there's completed variables, then we gain sql search time even if all the submitted columns are UNIQUE
        foreach (get_object_vars($this) as $variable => $variable_value){
            if ($variable_value != null && strcmp($variable, 'exists') != 0) {
                $where[$variable] = $variable_value;
            }
        }

        if (count($where) == 0){
            return false;
            //throw new KangorooException('There\'s no pointer to search this order.');
        }

        $returned_data = SQLUtil::select(ORDERS_TABLE, ['*'], $where, 'limit 1');
        if (count($returned_data) > 0){
            if ($rebind){
                $pointer = $returned_data[0];

                $this->unique_id = $pointer[self::$allowed_variables[0]];
                $this->provider_unique_id = $pointer[self::$allowed_variables[1]];
                $this->feature = json_decode($pointer[self::$allowed_variables[2]]);
                $this->status_history = self::stdObjects_array_status_history_to_order_status_array(json_decode($pointer[self::$allowed_variables[3]]));
                $this->year = $pointer[self::$allowed_variables[4]];
            }
            $this->exists = true;
        }

        return $this->exists;
    }

    private function get_files($whom){
        if (!$this->exists){
            return false;
        }

        $path = sprintf(UPLOADS_PROVIDER_SPECIFIC_YEAR_PATH, $this->provider_unique_id, date('Y')).$whom;
        if (!is_dir($path))
        {
            throw new KangorooException('Path doesn\'t exists');
        }

        $files = scandir($path);
        foreach ($files as $key => $link) {
            if(is_dir($path.$link)){ //ignore directories
                unset($files[$key]);
            }
        }
        return $files;
    }

    public function get_provider_files(){
        return $this->get_files('provider');
    }

    public function get_staff_files(){
        return $this->get_files('staff');
    }

    public static function get_current_year_order($provider_unique_id){
        $results = SQLUtil::select(ORDERS_TABLE,
            ['*'],

            [self::$allowed_variables[1] => $provider_unique_id,
                self::$allowed_variables[4] => date('Y')], 'limit 1');

        return self::raw_order_to_obj($results);
    }

    public static function current_year_order_is_in_process($provider_unique_id){
        /*$order = self::get_current_year_order($provider_unique_id);
        if ($order == null){
            //throw new OrderDoesntExists('');
            return false;
        }

        if (!$order instanceof Order){
            //throw new KangorooException('Parameter $order must be an instance of Order.class');
            return false;
        }*/

        $order = new Order(
            [
                'provider_unique_id' => $provider_unique_id,
                'year' => date('Y')
            ]
        );

        if (!$order->exists(true)){
            return false;
        }

        $last_status = $order->get_last_status();
        if ($last_status !== OrderStatusEnum::waiting_for_provider && $last_status !== OrderStatusEnum::completed_waiting_for_next_year){
            return $order;
        }
        return false;
    }

    public static function current_year_order_is_done($provider_unique_id){
        $order = self::get_current_year_order($provider_unique_id);
        if ($order == null){
            throw new OrderDoesntExists('');
        }

        if (!$order instanceof Order){
            throw new KangorooException('Parameter $order must be an instance of Order.class');
        }

        return $order->get_last_status()->status == OrderStatusEnum::completed_waiting_for_next_year;
    }

    public static function get_past_year_order($provider_unique_id){
        $results = SQLUtil::select(ORDERS_TABLE,
            ['*'],
            [
                self::$allowed_variables[1] => $provider_unique_id,
                //self::$allowed_variables[2] => OrderStatusEnum::completed_waiting_for_next_year,
                self::$allowed_variables[4] => (date('Y') - 1)
            ], 'limit 1');

        return self::raw_order_to_obj($results);
    }

    public static function get_past_years_orders($provider_unique_id){
        $results = SQLUtil::select(ORDERS_TABLE,
            ['*'],
            [
                self::$allowed_variables[1] => $provider_unique_id,
                //self::$allowed_variables[2] => OrderStatusEnum::completed_waiting_for_next_year,
                self::$allowed_variables[4].':<' => date('Y')
            ]);

        return self::raw_orders_to_obj($results);
    }

    /**
     * Get last order's status by comparing timestamp.
     * @return OrderStatus
     */
    public function get_last_status(){
        $history = $this->status_history;

        $comparison_status = new OrderStatus();
        $comparison_status->timestamp = 0;

        foreach ($history as $status){
            if ($status->timestamp > $comparison_status->timestamp){
                $comparison_status = $status;
            }
        }

        return $comparison_status;
    }

    public static function get_pending_orders(){
        $results = SQLUtil::select(ORDERS_TABLE,
            ['*'],

            [self::$allowed_variables[2] => OrderStatusEnum::waiting_for_provider,
                self::$allowed_variables[4] => date('Y')]);

        return $results;
    }

    /**
     * Upload files from providers / admins to respective directory associated with order unique id.
     * An upload path would be : /../uploads/X(10)/provider/data.x
     * /../uploads/X(10)/staff/data.x
     */
    private function upload_securely_files($whom, $files){
        $upload_path = sprintf(UPLOADS_PROVIDER_SPECIFIC_YEAR_PATH, $this->provider_unique_id, date('Y')).$whom.'/';

        if (!is_dir($upload_path)){
            mkdir($upload_path, 0777, true);
        }

        $max_file_size = (100 * pow(1024, 2));
        $count = count($files["name"]);

        for ($i = 0; $i < $count; $i++)
        {
            $file_name = $files['name'][$i];
            $file_temp_name = $files['tmp_name'][$i];
            $file_type = $files['type'][$i]; //verify from allowed files, can be faked, double check by mime type ?
            $file_size = $files['size'][$i]; //can't be trust, client can modify the value himself
            $file_upload_error = $files['error'][$i];

            if (!preg_match("`^[-0-9A-Z_\.]+$`i", $file_name) || mb_strlen($file_name, "UTF-8") > 225){
                throw new KangorooException(sprintf('Le fichier %s possède un nom non conforme.', $file_name));
            }

            /*if (!in_array(self::$allowed_file_types, $file_type)){
                throw new KangorooException(sprintf('Le fichier %s possède un type de fichier non autorisé.', $file_name));
            }*/

            if ($file_size > $max_file_size){ //max = 100Mo
                throw new KangorooException(sprintf('Le fichier %s a une taille supérieur à 100Mo.', $file_name));
            }

            if ($file_upload_error != UPLOAD_ERR_OK){
                throw new KangorooException(sprintf('Le fichier %s n\'a pas pu être upload pour l\'erreur suivante : %s', $file_name, $file_upload_error));
            }

            $upload_file = $upload_path.basename($file_name);
            if (!move_uploaded_file($file_temp_name, $upload_file)){
                throw new KangorooException(sprintf('Le fichier %s n\'a pas pu être upload pour une raison inconnu, veuillez rééssayer.', $file_name));
            }
        }

        return true;
    }

    public function upload_provider_files($files){
        return $this->upload_securely_files('provider', $files);
    }

    public function upload_staff_files($files){
        return $this->upload_securely_files('staff', $files);
    }

    /**
     * Translate SQL raw order object (stdObject) to Order object.
     * @param $raw
     * @return Order|null
     */
    private static function raw_order_to_obj($raw){
        $raw_order = (count($raw) > 0 ? $raw[0] : null);

        $order_vars = array();
        if ($raw_order != null){
            foreach ($raw_order as $key => $value){
                $order_vars[$key] = $value;
            }
        }

        return $order = ($raw_order != null ? new Order($order_vars) : null);
    }

    /**
     * Translate SQL raw orders objects (stdObject) to Order object array.
     * @param $raw
     * @return array
     */
    private static function raw_orders_to_obj($raw){
        $orders = array();
        $order_vars = array();
        for ($i = 0; $i < count($raw); $i++) {
            foreach ($raw[$i] as $key => $value) {
                $order_vars[$key] = $value;
            }
            $order = new Order($order_vars);
            $order->exists = true;

            array_push($orders, $order);
            $order_vars = array(); //reset
        }

        return $orders;
    }

    private static function stdObjects_array_status_history_to_order_status_array($std_array){
        $order_status_array = array();
        foreach ($std_array as $status){
            $temp_order_status = new OrderStatus(); //stdObject to OrderStatus
            $temp_order_status->status = $status->status;
            $temp_order_status->timestamp = $status->timestamp;

            array_push($order_status_array, $temp_order_status);
        }

        return $order_status_array;
    }
}