<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/03/2018
 * Time: 15:32
 */
abstract class UserType
{
    const provider = 0;
    const admin = 1;
}