<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 30/03/2018
 * Time: 19:53
 */

require_once(__DIR__.'/../util/SQLUtil.class.php');
require_once(__DIR__.'/../initializer.inc.php');

class UserSession
{
    public $_session_death = 3600; //1h (s)
    //public $_session = array();
    private $_db;

    public function __construct(){
        ini_set('session.save_handler', 'user');

        session_set_save_handler(array($this, 'open'),
            array($this, 'close'),
            array($this, 'read'),
            array($this, 'write'),
            array($this, 'destroy'),
            array($this, 'gc'));
//header('Authorization: Bearer x');
        session_start();
    }

    public function open(){
        $this->_db = SQLUtil::open();;
        $this->gc(); //nettoyage vieilles sessions
        return true;
    }

    public function close(){
        if ($this->_db == null || !$this->_db instanceof mysqli){ //integrity check
            //throw
            throw new DatabaseConnectionException('');
        }

        return SQLUtil::close($this->_db);
    }

    public function read($id)
    {
        $db = $this->_db;
        if ($db == null || !$db instanceof mysqli) { //integrity check
            //throw
            throw new DatabaseConnectionException('');
        }

        $data = SQLUtil::select(USERS_SESSIONS_TABLE, ['s_data'], ['s_id' => $id]);

        return (count($data) > 0 ? stripcslashes($data[0]['s_data']) : '');
    }

    public function write($id, $data){
        $db = $this->_db;
        if ($db == null || !$db instanceof mysqli){ //integrity check
            //throw
            throw new DatabaseConnectionException('');
        }

        $data_expiration = (time() + $this->_session_death);
        $data = $db->real_escape_string($data); //+id, si id changé depuis cookie injection possible?
        //var_dump($data);
        $insert = 'replace into '.USERS_SESSIONS_TABLE.' values(?,?,?)';
        if ($stmt = $db->prepare($insert)){
            $stmt->bind_param('ssd',
                $id,
                $data,
                $data_expiration);

            $stmt->execute();
            $stmt->close();
        }else{
            throw new DatabaseInvalidQueryException('');
        }

        return true;
    }

    public function destroy($id){
        $db = $this->_db;
        if ($db == null || !$db instanceof mysqli){ //integrity check
            //throw
            throw new DatabaseConnectionException('');
        }

        SQLUtil::delete(USERS_SESSIONS_TABLE, ['s_id' => $id]);
        return true;
    }

    public function gc(){
        $db = $this->_db;
        if ($db == null || !$db instanceof mysqli){ //integrity check
            //throw
            throw new DatabaseConnectionException('');
        }

        SQLUtil::delete(USERS_SESSIONS_TABLE, ['s_death:<' => 'unix_timestamp(now())']);
        return true;
    }
}