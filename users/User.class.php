<?php
require_once(__DIR__.'/../util/SQLUtil.class.php');

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 20/03/2018
 * Time: 19:45
 */
class User
{
    private $unique_id;
    private $type;
    private $provider;
    private $username;
    private $password;
    private $last_connection;

    private $exists = false;

    protected static $allowed_variables_names = array('unique_id', 'type', 'provider', 'username', 'password', 'last_connection');

    public function __construct($array){
        foreach ($array as $key=>$value){
            if (in_array($key, self::$allowed_variables_names)){
                switch($key){
                    case self::$allowed_variables_names[0]:
                        $this->unique_id = $value;
                        break;

                    case self::$allowed_variables_names[1]:
                        $this->type = $value;
                        break;

                    case self::$allowed_variables_names[2]:
                        $this->provider = $value;
                        break;

                    case self::$allowed_variables_names[3]:
                        $this->username = $value;
                        break;

                    case self::$allowed_variables_names[4]:
                        $this->password = $value;
                        break;

                    case self::$allowed_variables_names[5]:
                        $this->last_connection = $value;
                        break;
                }
            }else{
                throw new KangorooException('Argument "'.$key.'" isn\'t allowed.');
            }
        }
    }

    public function exists($rebind = false){
        $where = array(); //we adjust research as much as there's completed variables, then we gain sql search time even if all the submitted columns are UNIQUE
        foreach (get_object_vars($this) as $variable => $variable_value){
            if ($variable_value != null) {
                $where[$variable] = $variable_value;
            }
        }

        $returned_data = SQLUtil::select(USERS_TABLE, ['*'], $where, 'limit 1');
        if (count($returned_data) > 0){
            if ($rebind){
                $pointer = $returned_data[0];

                $this->unique_id = $pointer[self::$allowed_variables_names[0]];
                $this->type = $pointer[self::$allowed_variables_names[1]];
                $this->provider = $pointer[self::$allowed_variables_names[2]];
                $this->username = $pointer[self::$allowed_variables_names[3]];
                $this->password = $pointer[self::$allowed_variables_names[4]];
                $this->last_connection = $pointer[self::$allowed_variables_names[5]];
            }
            $this->exist = true;
        }

        return $this->exists;
    }

    /**
     * Se connecter.
     * @param $mail
     * @param $password
     * @param $remember
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     * @throws Exception
     */
    public static function login($username, $password, $remember){
        if (empty($username)) {
            throw new KangorooException('Aucun nom d\'utilisateur saisi.');
        }

        if (empty($password)) {
            throw new KangorooException('Aucun mot de passe saisi.');
        }

        //$username = strtolower($username);
        $returned_data = SQLUtil::select(USERS_TABLE,
            [
                self::$allowed_variables_names[0],
                self::$allowed_variables_names[2],
                self::$allowed_variables_names[4]],
            [
                self::$allowed_variables_names[3] => $username
            ],
            'limit 1');

        if (count($returned_data) > 0){
            $real_hashed_password = $returned_data[0][self::$allowed_variables_names[4]];
            if (!password_verify($password, $real_hashed_password)){
                throw new KangorooException('Mot de passe incorrect.');
            }

            $id = $returned_data[0][self::$allowed_variables_names[0]];

            $_SESSION['loggedIn'] = true;
            $_SESSION['unique_id'] = $id;
            $_SESSION['username'] = $username;
            $_SESSION['remember'] = $remember;

            return true;
        }else{
            throw new KangorooException('Aucun compte trouvé dans nos fichiers avec ces données.');
        }
    }

    /*/**
     * Enregistrement d'un nouvel customer.
     * @param $mail
     * @param $password
     * @param $password_confirm
     * @param $ip
     * @param string $firstname
     * @param string $surname
     * @param string $addresses
     * @param string $phone
     * @param string $birthday
     * @param int $gender
     * @param null $page2redirect
     * @return JsonResponse|string
     * @throws DataBadException
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     * @throws Exception
     * @throws UserFakeMailException
     * @throws UserLoginEmptyMailException
     * @throws UserLoginEmptyPasswordException
     * @throws UserRegisterAccountAlreadyExistException
     * @throws UserRegisterPasswordNEqualsException
     * @throws UserRejectedPasswordRegexException
     */
    /*public static function register($mail, $password, $password_confirm, $ip, $firstname = '', $surname = '', $addresses = '', $phone = '', $birthday = '', $gender = UserGender::MALE){
        if (empty($mail)) {
            throw new UserLoginEmptyMailException(L::form_empty_mail);
        }

        if (empty($password)) {
            throw new UserLoginEmptyPasswordException(L::form_empty_password);
        }

        if (empty($password_confirm)){
            throw new Exception('missing data');
        }

        //verification taille données à rajouter, sinon erreur sql lors de l'insertion

        if (fakeMail($mail)){
            throw new UserFakeMailException(L::form_fake_mail);
        }

        if (count($mail) > 50 || count($password) > 128 || count($password_confirm) > 128){
            throw new UserInputLengthReachedMaxException("Veuillez essayer d'autres identifiants.");
        }

        if (!passwordMatch($password)){
            throw new UserRejectedPasswordRegexException(L::form_password_regex);
        }

        if ($password !== $password_confirm) {
            throw new UserRegisterPasswordNEqualsException(L::form_passwords_not_equals);
        }

        $mail = strtolower($mail);

        $exist = self::user_exist($mail);
        if ($exist){
            throw new UserRegisterAccountAlreadyExistException(printf(L::form_account_exist, $mail));
        }

        $unique_id = uniqid();
        $user_type = DEFAULT_MEMBER_USER;
        $password = self::generate_most_optimal_password_hash($password);
        $now = time();

        SQLUtil::insert(USERS_TABLE, [
            'id' => $unique_id,
            'user_type' => $user_type,
            'firstname' => $firstname,
            'surname' => $surname,
            'mail' => $mail,
            'password' => $password,
            'addresses' => $addresses,
            'phone' => $phone,
            'ip' => $ip,
            'since' => $now,
            'last_connection' => $now,
            'birthday' => $birthday,
            'genre' => $gender]);

        //verification process start here
        $verification_key = Gateway::add_entry(GatewayType::verify, $unique_id);

        //send verification mail
        verificationMail($mail, $surname, $_SERVER['SERVER_NAME'].'/index.php?page=gateway&vkey=' .$verification_key
            .'&uid='
            .$unique_id
            .'&type='
            .GatewayType::verify);

        return true;
    }*/

    /**
     * Basically the same as verify() but for forgotten password.
     * @param $verification_key
     * @param $user_id
     * @return bool
     * @throws DataBadException
     * @throws DataMissingException
     * @throws DatabaseInvalidQueryException
     * @throws MedeliceException
     */
    public static function has_asked_for_password_reset($verification_key, $user_id){
        if (empty($verification_key) || empty($user_id)){
            throw new DataMissingException(L::data_missing);
        }

        if (strlen($verification_key) != 64){
            throw new DataBadException(L::data_bad);
        }

        if (strlen($user_id) != 13){
            throw new DataBadException(L::data_bad);
        }

        $returned_data = SQLUtil::select(GATEWAY_TABLE, ['*'], ['verification_key' => $verification_key, 'user_id' => $user_id, 'verification_type' => GatewayType::forgotten_password]);
        if (count($returned_data) > 0){
            $verification_stamp = $returned_data[0]['verification_stamp'];

            if (time() > $verification_stamp){
                SQLUtil::delete(GATEWAY_TABLE, ['verification_key' => $verification_key, 'user_id' => $user_id]);

                throw new MedeliceException(L::gateway_link_expired);
            }

        }else{
            throw new MedeliceException(L::gateway_not_asked);
        }

        return true;
    }

    /**
     * Disconnect user.
     * @throws DatabaseInvalidQueryException
     */
    public static function disconnect(){
        if (!isset($_SESSION['mail'])){
            echo "You're not connected"; //TODO
        }

        if (self::isLoggedByPersistentAuth()){
            $user = new User($_SESSION['mail']);
            self::revokePersistentAuth($user->id());
        }

        session_unset();
        $_SESSION = array();
        session_destroy();

        $_SESSION['loggedIn'] = false;
        $_SESSION['firstvisitoftheday'] = false;
    }

    /**
     * Check if user is logged.
     * @return bool
     */
    public static function isLogged(){
        return (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn']);
    }

    /**
     * Check if user is logged and logged by persistent auth system.
     * @return bool
     */
    public static function isLoggedByPersistentAuth(){
        return self::isLogged() && isset($_SESSION['loggedByPAuth']) && $_SESSION['loggedByPAuth'];
    }

    /**
     * Check if user possesses a persistent auth cookie.
     * @return bool|mixed
     */
    public static function hasPersistentAuth(){
        if (!isset($_COOKIE['medpauth'])){
            return false;
        }
        return $_COOKIE['medpauth'];
    }

    /**
     * Check if user possesses a valid auth cookie according to his information.
     * Return unique id if user has a valid valid persistent auth data otherwise false.
     * @return bool|mixed
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function hasValidPersistentAuth(){
        $cookie = self::hasPersistentAuth();
        if ($cookie == false){
            return false;
        }

        if (strlen($cookie) != 80){
            return false;
        }

        /*$in_random_hash = substr($cookie, 0, 64);
        $in_browser_hash = substr($cookie, 64, 8);
        $in_ip_hash = substr($cookie, 72, 8);*/

        $db_check = self::hasValidPersistentAuthInDB($cookie);
        return ($db_check != null ? $db_check : false);
    }

    /**
     * Check from database if user has a valid persistent auth.
     * Return unique id if valid persistent auth, otherwise null.
     * @param $unique_hash
     * @return string If == null : invalid persistent auth, != null : valid (unique_id)
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    private static function hasValidPersistentAuthInDB($unique_hash){
        $lifetime = time();
        $unique_id = null;
        $sql = SQLUtil::open();
        $query = 'select unique_id from users_persistent_auth where unique_hash = ? and lifetime >= ?';
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param('sd', $unique_hash, $lifetime);
            $stmt->bind_result($id);
            $stmt->execute();

            while ($stmt->fetch()){
                $unique_id = $id;
            }

            $stmt->close();
        }else{
            //throw new DatabaseInvalidQueryException('');
        }
        return $unique_id;
    }

    /**
     * Insert in database and set cookie for persistent auth.
     * @param bool|false $set_cookie
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     * @throws Exception
     */
    public static function setPersistentAuth($set_cookie = false){
        if (!self::isLogged()){
            throw new Exception('Can\'t set persistent auth without being connected');
        }

        $profile = new User($_SESSION['mail']);
        $id = $profile->id();

        //Generate hash
        $random_hash = hash("sha256", openssl_random_pseudo_bytes(10));
        $browser_hash = hash("crc32", $_SERVER['HTTP_USER_AGENT']);
        $ip_hash = hash("crc32", $_SERVER['REMOTE_ADDR']);
        $hash = $random_hash.$browser_hash.$ip_hash;

        $lifetime = (time() + (86400*14)); //2w
        if ($set_cookie){ //if set, must be in the initializer
            // if (!isset($_SESSION['remember_cookie_set']) || $_SESSION['remember_cookie_set'] != true)
            setcookie('medpauth', $hash, $lifetime, '/', (DEBUG ? '.medelice.dev' : '.medelice.com'), true, true);

            $_SESSION['remember_cookie_set'] = false;
        }

        SQLUtil::insert('users_persistent_auth', ['unique_id' => $id, 'unique_hash' => $hash, 'lifetime' => $lifetime]);
    }

    /**
     * Login remotely by persistent cookie.
     * @return bool True if successfully logged.
     * @throws DatabaseConnectionException
     * @throws UserAccountExistenceException
     */
    public static function loginByPersistentAuth()
    {
        $unique_id = self::hasValidPersistentAuth();
        if (is_bool($unique_id) && !$unique_id) {
            return false;
        }

        $profile = self::get_user_from('id', $unique_id);
        if ($profile == null) {
            throw new UserAccountExistenceException(printf(L::form_account_not_exist_id, $unique_id));
        }

        $_SESSION['loggedIn'] = true;
        $_SESSION['loggedByPAuth'] = true;
        $_SESSION['mail'] = $profile->mail();
        $_SESSION['firstname'] = $profile->firstname();
        $_SESSION['surname'] = $profile->surname();
        $_SESSION['loggedByPAuth'] = true;
        //$_SESSION['persistent'] = true;
        //$_SESSION['remember'] = true;

        self::retrieve_and_set_to_session_saved_cart($unique_id);

        return true;
    }

    /**
     * Revoke a persistent auth session from DB. No cookie security here, only DB.
     * @param $unique_id
     * @return bool If true, persistent auth's revoked.
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function revokePersistentAuth($unique_id){
        /*if ($login_check && !self::isLogged()){
            throw new Exception('Can\'t revoke persistent auth without being connected');
        }*/

        /*if ($cookie_check && !self::hasPersistentAuth()){
            throw new Exception('Can\'t revoke persistent auth without possesses a valid cookie');
        }*/

        /*$profile = new User($_SESSION['mail']);
        $id = $profile->id();*/

        $hash = null;
        $sql = SQLUtil::open();
        $select = 'select unique_hash from users_persistent_auth where unique_id = ?';
        if ($stmt = $sql->prepare($select)){
            $stmt->bind_param('s', $unique_id);
            $stmt->bind_result($out_hash);
            $stmt->execute();

            while ($stmt->fetch()){
                $hash = $out_hash;
            }
            $stmt->close();
        }else{
            //throw new DatabaseInvalidQueryException('');
        }

        if ($hash == null)
            return false;

        $delete = 'delete from users_persistent_auth where unique_hash = ?';
        if ($stmt = $sql->prepare($delete)){
            $stmt->bind_param('s', $hash);
            $stmt->execute();
            $stmt->close();
        }else{
            //throw new DatabaseInvalidQueryException('');
        }

        return true;
    }

    /**
     * Check if user's logged by persistent auth and also logged with a valid cookie, if yes
     * we refresh the unique_id for each request, if not we disconnect him.
     */
    public static function watchdogPersistentAuthCookieIntegrity(){
        if (!self::isLoggedByPersistentAuth()){
            return;
        }

        if (self::hasValidPersistentAuth()){
            self::setPersistentAuth(true); //refresh the id
            return;
        }

        self::disconnect();
    }

    private static function generate_most_optimal_password_hash($password){
        $timeTarget = 0.05;
        $cost = 8; //Default cost
        do{
            $cost++;
            $start = microtime(true);
            $hashed_password = password_hash($password, PASSWORD_BCRYPT, ["cost" => $cost]);
            $end = microtime(true);
        }while (($end - $start) < $timeTarget);

        return $hashed_password;
    }

    public static function is_admin($user){
        return $user->type == UserType::admin;
    }

    public static function get_uid_from_session(){
        return $_SESSION['unique_id'];
    }
}