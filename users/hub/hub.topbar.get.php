<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 30/03/2018
 * Time: 20:27
 */

?>


<div class="hub-topsection">
    <div class="hub-logosection">
        <img class="hub-logo" src="/images/temp_banner.jpg">
        <img class="hub-disconnect" src="/images/disconnect.svg" title="Déconnexion" alt="Disconnect">
        <div class="hub-logosection-datetime" onload="printTime(true)">
        </div>
    </div>
    <div class="hub-navsection">
        <a class="hub-navsection-button" href="/index.php?page=hub&section=myorder" style="float: left;">
            Ma commande
        </a>

        <a class="hub-navsection-button" href="/index.php?page=hub&section=lastorders" style="float: left;">
            Mes anciennes commandes
        </a>

        <a class="hub-navsection-button" href="/index.php?page=hub&section=myemails" style="float: right;">
            Mon courrier
        </a>
    </div>
</div>
