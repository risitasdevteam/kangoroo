<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 24/03/2018
 * Time: 08:53
 */
require_once(__DIR__.'/../../../orders/Order.class.php');

if (!User::isLogged()){
    http_response_code(401);
    return;
}

$provider_unique_id = User::get_uid_from_session();

$last_orders = Order::get_past_years_orders($provider_unique_id);

foreach ($last_orders as $order){
    if (!($order instanceof Order)){
        break;
    }

    //Affichage données sous forme de tableau
}