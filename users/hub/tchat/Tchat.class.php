<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 08/04/2018
 * Time: 11:04
 */
class Tchat
{
    public $unique_id;
    public $sender_unique_id;
    public $content;
    public $timestamp;

    protected static $allowed_variables_names = array('unique_id', 'sender_unique_id', 'content', 'timestamp');

    public function construct($array){
        foreach ($array as $key=>$value){
            if (in_array($key, self::$allowed_variables_names)){
                switch($key){
                    case self::$allowed_variables_names[0]:
                        $this->unique_id = $value;
                        break;

                    case self::$allowed_variables_names[1]:
                        $this->sender_unique_id = $value;
                        break;

                    case self::$allowed_variables_names[2]:
                        $this->content = $value;
                        break;

                    case self::$allowed_variables_names[3]:
                        $this->timestamp = $value;
                        break;
                }
            }else{
                throw new KangorooException("L'argument $key n'est pas valide.");
            }
        }

        if (!isset($this->unique_id)){
            $this->unique_id = $this->generate_unique_id();
        }

        if (!isset($this->timestamp)){
            $this->timestamp = time();
        }
    }

    public function generate_unique_id(){
        return bin2hex(openssl_random_pseudo_bytes(5));
    }
}