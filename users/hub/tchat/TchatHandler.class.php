<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 08/04/2018
 * Time: 11:03
 */
require_once(__DIR__.'/Tchat.class.php');
require_once(__DIR__.'/../../../util/Misc.php');

class TchatHandler
{
    public static function send_msg($sender_unique_id, $msg){
        $tchat = new Tchat([
            "sender_unique_id" => $sender_unique_id,
            "content" => $msg
        ]);


        SQLUtil::insert(TCHAT_TABLE, object_to_array($tchat));
    }

    //public static function retrieve
    public static function retrieve_in_period_before($sender_unique_id, $period){
        $messages = self::retrieve($sender_unique_id);

        $timestamp = strtotime($period);
        if (!$timestamp){
            return null;
        }

        if ($timestamp > time()){
            return null;
        }

        $returned_messages = [];
        foreach ($messages as $tchat){
            if (!$tchat instanceof Tchat){
                break;
            }

            if ($tchat->timestamp < $timestamp){
                array_push($returned_messages, $tchat);
            }
        }

        return $returned_messages;
    }

    public static function retrieve($sender_unique_id){
        $data = SQLUtil::select(TCHAT_TABLE, ['*'], [
            "sender_unique_id" => $sender_unique_id
        ]);

        return self::raw_tchat_to_obj($data);
    }

    /**
     * Translate SQL raw Tchat objects (stdObject) to Tchat object array. In order to make a constructor from an array.
     * @param $raw
     * @return array
     */
    private static function raw_tchat_to_obj($raw){
        $messages = array();
        $message_vars = array();
        for ($i = 0; $i < count($raw); $i++) {
            foreach ($raw[$i] as $key => $value) {
                $message_vars[$key] = $value;
            }
            $message = new Tchat($message_vars);

            array_push($messages, $message);
            $message_vars = array(); //reset
        }

        return $messages;
    }
}