<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 22/03/2018
 * Time: 13:35
 */

require_once(__DIR__.'/../User.class.php');
/*
if (!User::isLogged()){
    exit; //redirect
}
*/
// ma commande, mes anciennes commandes, mon courier

$current_section = isset($_GET['section']) ? $_GET['section'] : "myorder";

include(__DIR__ . '/hub.topbar.get.php');

?>

<div class="hub-content-zone">
    <?php
        switch($current_section)
        {
            case 'myorder':
                include(__DIR__.'/my_order/my_order.get.php');
                break;

            case 'lastorders':
                include(__DIR__.'/my_last_orders/my_last_orders.get.php');
                break;

            case 'myemails':

                break;
        }
    ?>
</div>
