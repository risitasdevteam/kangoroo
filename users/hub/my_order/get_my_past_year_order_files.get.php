<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 31/03/2018
 * Time: 19:14
 */
require_once(__DIR__.'/../../User.class.php');
require_once(__DIR__.'/../../../orders/Order.class.php');
require_once(__DIR__.'/../../../util/RequestResponse.class.php');
require_once(__DIR__.'/../../../exceptions/KangorooException.php');

try{
    if (!User::isLogged()){
        throw new KangorooException('You need to be logged.');
    }

    $provider_unique_id = User::get_uid_from_session();
    $year = filter_input_and_throw_exception_if_invalid(INPUT_GET, 'year', FILTER_SANITIZE_NUMBER_INT);

    $order = new Order(["provider_unique_id" => $provider_unique_id, 'year' => $year]);
    if (!$order->exists(true))
    {
        throw new KangorooException('Order doesn\'t exists.');
    }

   var_dump($order->get_provider_files());
}
catch(Exception $e){
    exception_to_request_payload($e);
}