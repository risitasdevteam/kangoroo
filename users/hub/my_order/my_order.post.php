<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 24/03/2018
 * Time: 08:52
 */
require_once(__DIR__.'/../../../exceptions/KangorooException.php');
require_once(__DIR__.'/../../../orders/Order.class.php');
require_once(__DIR__.'/../../User.class.php');
require_once(__DIR__.'/../../../util/RequestResponse.class.php');

try{
    $id = User::get_uid_from_session();

    $provider_files = $_FILES['provider_files'];
    if (!isset($provider_files)) {
        throw new KangorooException("Aucun fichier n'a été envoyé.");
    }

    $feature = filter_input_and_throw_exception_if_invalid(INPUT_POST, 'feature', FILTER_SANITIZE_NUMBER_INT, false);
    $feature_msg = filter_input_and_throw_exception_if_invalid(INPUT_POST, 'feature_msg', FILTER_SANITIZE_STRING);

    $final_feature_array = array($feature, $feature_msg);

    $order = new Order([
        "provider_unique_id" => $id,
        "feature" => $final_feature_array
    ]);

    $has_been_created = Order::create($order, $provider_files);
    if ($has_been_created){
        $response = new RequestResponse(RequestResponseStatusType::OK, new RequestResponsePayload("msg", "Commande prise en compte avec succès, prochaine étape : validation des données par les administrateurs"));
        $response->transmit();
    }
}
catch(Exception $e){
    exception_to_request_payload($e);
}