<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 24/03/2018
 * Time: 08:35
 */

require_once(__DIR__.'/../../../orders/OrderFeature.enum.php');
require_once(__DIR__.'/../../User.class.php');
require_once(__DIR__.'/../../../orders/Order.class.php');

$logged = User::isLogged();
if (!$logged){
    exit;
}

$id = User::get_uid_from_session();

$current_year_order_is_in_process = Order::current_year_order_is_in_process($id);
if ($current_year_order_is_in_process instanceof Order) {
    //affichage de l'historique des status

    $status_history = $current_year_order_is_in_process->status_history;
    foreach ($status_history as $status) {
        if (!$status instanceof OrderStatus) {
            break;
        }

        echo $status->status;
    }
}

$past_years_orders = Order::get_past_years_orders($id);

//Je souhaite réutiliser les fichiers des années précédentes (
//Je souhaite mettre en ligne de nouveaux fichiers
?>

<form class="my_order_fom" method="post" action="/users/hub/my_order/my_order.post.php" enctype="multipart/form-data">
    <input type="button" name="use_last_years_files" value="Je souhaite utiliser les fichiers des années précédentes (modifications possibles)"/><br> <!--SSI le client a eu des fichiers à au moins (a-1) -->
    <input type="button" name="upload_new_files" value="Je souhaite mettre en ligne de nouveaux fichiers"/><br>

    <div class="order_based_on_last_data" style="border-style: solid">
        <h1>Commande à partir d'anciens fichiers.</h1> <!-- Temporaire -->
        <label for="last_data_combobox">Année ?</label>
        <select name="last_data_combobox">
            <?php
                if ($past_years_orders != null){
                    foreach ($past_years_orders as $order){
                        if (!$order instanceof Order){
                        }

                        $year = $order->year;
                        echo '<option value="'.$year.'|'.$order->feature.'">'.$year.'</option>';
                    }
                }
            ?>
        </select>
        <script>
            var year_combo_selector = $('[name="last_data_combobox"]');

            $( document ).ready(function() {
                select_format_depending_of_year(year_combo_selector);
            });

            year_combo_selector.change(function(){
                select_format_depending_of_year($(this));
            });

            function select_format_depending_of_year(selector){
                var feature_value = selector.val().split("|")[1];
                $('.order_based_on_last_data [name="feature"][value="' + feature_value + '"]').prop("checked", true);
            }
        </script>

        <p>Format ?</p>
        <label for="feature_1">1 page</label>
        <input type="radio" name="feature" id="feature_0" value="<?php echo OrderFeature::one_full_page ?>"/><br>

        <label for="feature_2">1/2 page</label>
        <input type="radio" name="feature" id="feature_1" value="<?php echo OrderFeature::a_half_page ?>"/><br>

        <label for="feature_3">1/4 page</label>
        <input type="radio" name="feature" id="feature_2" value="<?php echo OrderFeature::a_quarter_page ?>"/><br>
    </div><br>

    <div class="order_based_on_new_data" style="border-style: solid">
        <h1>Nouvelle commande</h1> <!-- Temporaire -->
        <label for="feature_1">1 page</label>
        <input type="radio" name="feature" id="feature_0" value="<?php echo OrderFeature::one_full_page ?>"/><br>

        <label for="feature_2">1/2 page</label>
        <input type="radio" name="feature" id="feature_1" value="<?php echo OrderFeature::a_half_page ?>"/><br>

        <label for="feature_3">1/4 page</label>
        <input type="radio" name="feature" id="feature_2" value="<?php echo OrderFeature::a_quarter_page ?>"/><br>

        <label for="provider_files[]">Fichiers à saisir</label>
        <input name="provider_files[]" type="file" multiple="multiple"/><br>

        <label for="feature_msg">Message complémentaire</label>
        <textarea name="feature_msg" form="order_based_on_new_data" style="width:250px;height:150px;"></textarea>
    </div>
    <input type="submit">
</form>