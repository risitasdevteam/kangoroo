/**
 * Created by jsanglier on 20/03/2018.
 */

$('.login').submit(function(e){
    e.preventDefault();

    var url = "/users/login/login.post.php";
    var data2post = serializeForm($(this));

    post(url, data2post, function(){
        location.href = "index.php?page=hub";
    },function(){
    }, true);
});