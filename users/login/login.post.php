<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 20/03/2018
 * Time: 23:19
 */
require_once(__DIR__.'/../../util/Misc.php');
require_once(__DIR__.'/../User.class.php');
require_once(__DIR__.'/../../util/RequestResponse.class.php');

try{
    post_thrown();

    $username = filter_input_and_throw_exception_if_invalid(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = filter_input_and_throw_exception_if_invalid(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

    $logged = User::login($username, $password, false);
    if ($logged){
        $response = new RequestResponse(RequestResponseStatusType::OK, null);
        $response->transmit();
    }
}
catch(Exception $e){
    exception_to_request_payload($e);
}