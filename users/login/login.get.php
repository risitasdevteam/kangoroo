<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 20/03/2018
 * Time: 22:35
 */
?>

<div style="display: table; height: 100%; width: 100%;">
    <div style="position: absolute; width: 100%">
        <img style="width: 35%; margin: 5vw 32.5%" src="../../images/temp_banner.jpg">
    </div>
    <div style="display: table-cell; vertical-align: middle">
        <form style="width: 20vw; padding: 1.5vw; background-color: #ffffff; box-shadow: 0.0266vw 0.0532vw 0.0532vw #eeeeee; border: 0.0532vw solid rgba(200, 200, 200, 0.2); margin: auto; vertical-align: middle" class="login" method="post">
            <input class="form-login-field" style="" type="text" name="username" placeholder="Identifiant" />
            <input class="form-login-field" style="width: 85%; margin: 0 7.5% 0.5vw; height: 1.8vw; border: none; outline: none; font-size: 1vw; padding: 0 3%;" type="password" name="password" placeholder="Mot de passe" />
            <button style="width: 60%; height: 2.5vw; border-radius: 0.25vw; margin: 0.5vw 20% 0; border: none; outline: none; cursor: pointer; background-color: #1ba2be; color: white; font-size: 1vw; text-transform: uppercase" type="submit">Connexion</button>
        </form>
    </div>
</div>

<script src="/users/login/login.js"></script>