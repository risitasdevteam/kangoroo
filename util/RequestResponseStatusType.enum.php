<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 21/03/2018
 * Time: 13:51
 */

abstract class RequestResponseStatusType{
    const OK = "ok";
    const ERROR = "error";
}
