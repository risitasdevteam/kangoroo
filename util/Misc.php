<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 21/03/2018
 * Time: 13:53
 */
require_once(__DIR__.'/../exceptions/KangorooException.php');

/**
 * ~Exceptions manager
 * @param Exception $e
 */
function exception_to_request_payload(Exception $e)
{
    $traits = class_uses_deep($e);
    $response = new RequestResponse(RequestResponseStatusType::ERROR, null);
    if (isset($traits['ShowableException'])) {
        $response->add_payload(new RequestResponsePayload("error", $e->getMessage()));
    } else if (isset($traits['ConfidentialException'])){
        $response->add_payload(new RequestResponsePayload("error", "Don't panic.")); //confidential error, must send an email to dev, "Don't panic" returned by default.
        //technical_mail($e);
    }else{
        if ($e instanceof KangorooException){ //we might have forgotten to implement a specific trait or just mean there's no reason to create a specific exception
            $response->add_payload(new RequestResponsePayload("error", $e->getMessage()));
        }else{
            $response->add_payload(new RequestResponsePayload("error", "Don't panic."));
        }
    }
    $response->transmit();
}

function class_uses_deep($class, $autoload = true) {
    $traits = [];
    do {
        $traits = array_merge(class_uses($class, $autoload), $traits);
    } while($class = get_parent_class($class));
    foreach ($traits as $trait => $same) {
        $traits = array_merge(class_uses($trait, $autoload), $traits);
    }
    return array_unique($traits);
}

function post_thrown(){
    if ($_SERVER['REQUEST_METHOD'] != 'POST'){
        throw new KangorooException("It's not working like that...");
    }
}

function filter_input_and_throw_exception_if_invalid($input, $var_name, $filter, $ignore_if_not_submitted = true){
    $result = filter_input($input, $var_name, $filter);

    if ($result == null){
        if (!$ignore_if_not_submitted){
            throw new KangorooException("Can't put a filter on a null variable.");
        }
    }

    if (!$result){
        throw new KangorooException(sprintf("Filter returned an error for %s input.", $var_name));
    }
    return $result;
}

function object_to_array($obj){
    $vars = get_object_vars($obj);
    $array = [];
    foreach ($vars as $var_name => $var){
        $array[$var_name] = $var;
    }

    return $array;
}