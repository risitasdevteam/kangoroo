<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 21/03/2018
 * Time: 13:50
 */

require_once(__DIR__ . '/RequestResponseStatusType.enum.php');
require_once(__DIR__ . '/RequestResponsePayload.class.php');

/**
 * Json based response for POST/GET requests returns.
 * Always dealing with a status, a payload (which contains all the data) and a timestamp.
 * Class JsonResponse
 * @since Alpha 0.5
 */
class RequestResponse
{
    /**
     * The status of the response.
     * Defined from JsonResponseStatusType
     * @var int
     */
    private $status;
    /**
     * Array which carry all the returned data, view it like a util charge.
     * @var array
     */
    private $payload = array();
    /**
     * Unix timestamp, response issued at
     * @var int
     */
    private $iat;

    /**
     * JsonResponse constructor, must be instanced with all parameters.
     * @param $status int|RequestResponseStatusType
     * @param ...$in_payload RequestResponsePayload Put null for no payload.
     */
    public function __construct($status, ...$in_payload){
        $this->status = $status;

        foreach ($in_payload as $payload) {
            if ($payload != null)
                $this->payload[$payload->_key] = $payload->_value;
        }
        $this->iat = time();
    }

    /**
     * Add after instanced the constructor a new JsonResponsePayload
     * @param $payload RequestResponsePayload
     */
    public function add_payload($payload){
        if ($payload != null)
            $this->payload[$payload->_key] = $payload->_value;
    }

    /**
     * Get the json encoded version of the response.
     * @return string
     */
    public function encode(){
        //var_dump(get_object_vars($this));
        return json_encode(get_object_vars($this));
    }

    /**
     * Just echo the json encoded response.
     * Added lately
     */
    public function transmit(){
        echo $this->encode();
    }
}
