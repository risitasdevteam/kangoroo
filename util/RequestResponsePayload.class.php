<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 21/03/2018
 * Time: 13:51
 */


class RequestResponsePayload{

    public $_key;
    public $_value;

    public function __construct($key, $value){
        $this->_key = $key;
        $this->_value = $value;
    }
}
