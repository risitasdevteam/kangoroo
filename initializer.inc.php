<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 20/03/2018
 * Time: 19:10
 */

define('DEBUG', true);

if (DEBUG){
    error_reporting(E_ALL | E_STRICT);
    ini_set('display_errors', 'On');
    ini_set('log_errors', 'Off');
}else{
    error_reporting(0);
    ini_set('display_errors', 'Off');
    ini_set('log_errors', 'On');
    ini_set('error_log', '/Medelice.com/error_log');
}

if (DEBUG) {
    define('DB_ADDRESS', "localhost");
    define('DB_USER', "root");
    define('DB_PASSWORD', "root");
    define('DB', "kangoroo");
}
else{
    /*define('DB_ADDRESS', "medelicepebd.mysql.db");
    define('DB_USER', "medelicepebd");
    define('DB_PASSWORD', "5hSWLqCb6VKtDd4ckfqUMnd39qb57J");
    define('DB', "medelicepebd");*/
}

define('USERS_TABLE', "users");
define('ORDERS_TABLE', "orders");
define('USERS_SESSIONS_TABLE', 'users_sessions');
define('TCHAT_TABLE', "tchat");

require_once(__DIR__.'/util/SQLUtil.class.php');
require_once(__DIR__.'/util/Misc.php');
require_once(__DIR__.'/users/UserSession.php');

new UserSession();

if (session_status() == PHP_SESSION_ACTIVE) {
    if (!isset($_SESSION['loggedIn'])) {
        $_SESSION['loggedIn'] = false;
        //$_SESSION['remember'] = false;
    }
}