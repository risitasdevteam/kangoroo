/**
 * Created by jsanglier on 20/03/2018.
 */

function post(url, serialized_data, ok_status_callback, error_status_callback, standard_error_handler){
    return $.post(url, serialized_data).done(function(response){
        response = JSON.parse(response);
        switch (response.status){
            case 'ok':
                ok_status_callback.call(this, response);
                break;

            case 'error':
                error_status_callback(this, response);
                if (standard_error_handler == true)
                    alert(response.payload.error);

                break;
        }
    });
}

function serializeForm(jquery_selector){
    var array = jquery_selector.serializeArray();
    var twisted_array = {};
    for (var i = 0; i < array.length; i++){
        twisted_array[array[i].name] = array[i].value;
    }
    return twisted_array;
}

function printTime(used_at_page_load){
    $( document ).ready(function(){
        if (!used_at_page_load) {
            window.setInterval(function () {
                maestro();
            }, 1000);
        }
        else{
            maestro();
        }

        function formatSmallerThanTenWithZero(variable){
            return (variable < 10 ? "0" + variable : variable);
        }

        function maestro(){
            var date = new Date();

            var time = [date.getDate(), (date.getMonth() + 1), date.getFullYear(), date.getHours(), date.getMinutes(), date.getSeconds()];
            var time_str = "";
            for (var i = 0; i < time.length; i++){
                time[i] = formatSmallerThanTenWithZero(time[i]);

                if (i <= 2){
                    time_str += time[i] + (i == 2 ? " | " : "/");
                }else{
                    time_str += time[i] + (i == 5 ? "" : ":");
                }
            }

            $('.hub-logosection-datetime').get(0).innerHTML = time_str;
        }
    });
}

printTime();