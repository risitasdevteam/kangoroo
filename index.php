<?php
require_once(__DIR__.'/initializer.inc.php');

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 20/03/2018
 * Time: 23:32
 */
?>

<html>
    <head>
        <?php include('head_data.inc.php'); ?>
    </head>

    <body>
        <?php
        $current_page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);

        $include = ''; // surtout pas mettre index.php sinon boucle infinie + laisser vide pour éviter d'avoir un espace vide dégueu quand on a rien à include
        switch ($current_page)
        {
            case 'login':
                $include = '/users/login/login.get.php';
                break;

            case 'hub':
                $include = '/users/hub/hub.get.php';
                break;

            case 'my_order':
                $include = '/users/hub/my_order/my_order.get.php';
                break;

            default:
                $include = null; // ici on mettra une redirection vers une page 404 custom plus tard
                $current_page = '404';
                break;
        }
        ?>

        <div style="display: block; width: 100%; font-family: Montserrat, sans-serif, Arial">
            <?php
                if ($include != null || $include != '')
                    include(__DIR__.'/'.$include);
            ?>
        </div>
    </body>
</html>


<?php

?>